Static analysis for a simple imperative language, inspired by
[TIP](https://github.com/cs-au-dk/TIP).

## Installing

Prerequisites:

 * Java 11 (or later) -- https://openjdk.org/
 * Gradle 8.8 (or later) -- https://gradle.org/

We use [graphviz](https://www.graphviz.org/) for visualization of analysis
artifacts.

If you want to try out the analysis or examine its source code, clone the
repository and change directory like this:

    $ git clone https://gitlab.com/softwaretechnik-dhge/sca.git
    $ cd sca

You can then either directly run the tool using Gradle:

    $ gradle run

or package the tool first and run the tool afterwards, e.g.:

    $ gradle installDist
    $ export PATH=$PATH:$(pwd)/build/install/sca/bin
    $ sca

## Usage

Here's a simple example of using the analysis on the command line:

    $ sca examples/example.while

This command just formats the `example.while` file and writes the result to
standard output. For more advanced analyses, run the command without any
arguments to see all possible options. Option `-verbose` is recommended when
developing and testing analyses.

To generate intermediate representations, e.g., the abstract syntax tree, use
the following command line option:

    $ sca examples/example.while -ast

which will build .dot files for the chosen intermediate representations, from
which you can generate various image file formats, e.g., using:

    $ dot -Tpng examples/example.while.ast.dot > ast.png
