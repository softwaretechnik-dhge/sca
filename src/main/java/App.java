import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

import sca.ast.Program;
import sca.ast.utilities.ASTDotPrinter;
import sca.ast.utilities.Printer;
import sca.cfg.Builder;
import sca.cfg.IntraproceduralControlFlowGraph;
import sca.cfg.Node;
import sca.cfg.utilities.AnnotationDirection;
import sca.cfg.utilities.CFGDotPrinter;
import sca.cfg.utilities.CFGWithAnnotationsDotPrinter;
import sca.dfa.IntraproceduralBackwardAnalysis;
import sca.dfa.IntraproceduralDataFlowAnalysis;
import sca.dfa.RoundRobinSolver;
import sca.dfa.Solver;
import sca.dfa.WorklistSolver;
import sca.dfa.analyses.AnticipabilityAnalysis;
import sca.dfa.analyses.AvailableExpressionAnalysis;
import sca.dfa.analyses.Definition;
import sca.dfa.analyses.DominatorAnalysis;
import sca.dfa.analyses.LivenessAnalysis;
import sca.dfa.analyses.ReachingDefinitionAnalysis;
import sca.dfa.analyses.udchains.UDChains;
import sca.dfa.analyses.udchains.UDChainsDotPrinter;
import sca.parser.Parser;
import sca.parser.ParserException;
import sca.types.TypeChecker;
import sca.types.TypeCheckerException;

public class App {
    private static final Logger log = LogManager.getLogger(App.class);

    private static Options buildOptions() {
        final Options options = new Options();
        options.addOption(Option
                .builder("cfg")
                .desc("Create .dot file for control flow graphs.")
                .build());
        options.addOption(Option
                .builder("ast")
                .desc("Create .dot file for abstract syntax tree.")
                .build());
        options.addOption(Option
                .builder("verbose")
                .desc("Print warnings and diagnostics.")
                .build());
        options.addOption(Option
                .builder("annotate")
                .desc("Data flow annotations in control flow graphs.")
                .build());
        options.addOption(Option
                .builder("worklist")
                .desc("Use worklist instead of round-robin solver.")
                .build());
        options.addOption(Option
                .builder("dominators")
                .desc("Intraprocedural dominator analysis.")
                .build());
        options.addOption(Option
                .builder("definitions")
                .desc("Intraprocedural reaching definitions analysis.")
                .build());
        options.addOption(Option
                .builder("availability")
                .desc("Intraprocedural available expressions analysis.")
                .build());
        options.addOption(Option
                .builder("liveness")
                .desc("Intraprocedural live variables analysis.")
                .build());
        options.addOption(Option
                .builder("anticipability")
                .desc("Intraprocedural very busy expressions analysis.")
                .build());
        options.addOption(Option
                .builder("udchains")
                .desc("Variable use definition chains.")
                .build());
        return options;
    }

    private static void printUsage(Options options) {
        final HelpFormatter formatter = HelpFormatter.builder().get();
        formatter.printHelp("sca <FILE>",
                "Static analysis for a simple imperative language.\n\n",
                options,
                "\nHomepage: https://gitlab.com/softwaretechnik-dhge/sca",
                true);
    }

    private static void analyze(String option, String fileName, String fun,
            IntraproceduralControlFlowGraph cfg, CommandLine line,
            IntraproceduralDataFlowAnalysis<?> analysis, Solver solver) 
            throws IOException {
        log.info("Calculating " + option + " for " + fun + " ...");
        final Map<Node, ?> information = solver.solve(analysis);
        if (line.hasOption("cfg") && line.hasOption("annotate")) {
            log.info("Writing annotated control flow graph to .dot file ...");
            FileUtils.writeStringToFile(
                new File(fileName + "." + fun + ".cfg." + option + ".dot"),
                CFGWithAnnotationsDotPrinter.print(
                        cfg,
                        (analysis instanceof IntraproceduralBackwardAnalysis
                                ? AnnotationDirection.BACKWARD
                                : AnnotationDirection.FORWARD),
                        information),
                "UTF-8");
            log.info("Generated " + fileName + "." + fun
                    + ".cfg." + "option" + ".dot");
        }
    }

    public static void main(String[] args) {
        final CommandLineParser argParser = new DefaultParser();
        final Options options = buildOptions();
        try {
            if (args.length == 0) {
                printUsage(options);
                System.exit(0);
            }
            final CommandLine line = argParser.parse(options, args);
            if (line.getArgs().length != 1) {
                System.err.println("Exactly one file should be given.");
                System.exit(1);
            }
            final String fileName = line.getArgs()[0];
            final File file = new File(fileName);
            if (!file.isFile()) {
                System.err.println("Argument " + fileName + " is not a file.");
                System.exit(1);
            }
            if (line.hasOption("verbose")) {
                Configurator.setRootLevel(Level.DEBUG);
            }
            final String input = FileUtils.readFileToString(file, "UTF-8");
            final Solver solver =
                    line.hasOption("worklist")
                            ? new WorklistSolver()
                            : new RoundRobinSolver();
            final TypeChecker typechecker = new TypeChecker();
            final Parser parser = new Parser();
            log.info("Parsing file " + fileName + " ...");
            final Program ast = parser.parse(input);
            if (line.hasOption("ast")) {
                log.info("Writing abstract syntax tree to .dot file ...");
                FileUtils.writeStringToFile(
                    new File(fileName + ".ast.dot"),
                    ASTDotPrinter.print(ast),
                    "UTF-8");
                log.info("Created " + fileName + ".ast.dot");
            }
            log.info("Typechecking ...");
            typechecker.typecheck(ast);
            log.info("Generating intraprocedural control flow graphs ...");
            final Map<String, IntraproceduralControlFlowGraph> cfgMap =
                    Builder.build(ast);
            for (String function : cfgMap.keySet()) {
                IntraproceduralControlFlowGraph cfg = cfgMap.get(function);
                if (line.hasOption("cfg")) {
                    log.info("Writing control flow graph to .dot file ...");
                    FileUtils.writeStringToFile(
                        new File(fileName + "." + function + ".cfg.dot"),
                        CFGDotPrinter.print(cfg),
                        "UTF-8");
                    log.info("Created " + fileName + "." + function + ".cfg.dot");
                }
                if (line.hasOption("dominators")) {
                    analyze("dominators", fileName, function, cfg, line,
                            new DominatorAnalysis(cfg), solver);
                }
                if (line.hasOption("definitions")) {
                    analyze("definitions", fileName, function, cfg, line,
                            new ReachingDefinitionAnalysis(cfg), solver);
                }
                if (line.hasOption("availability")) {
                    analyze("availability", fileName, function, cfg, line,
                            new AvailableExpressionAnalysis(cfg), solver);
                }
                if (line.hasOption("anticipability")) {
                    analyze("anticipability", fileName, function, cfg, line,
                            new AnticipabilityAnalysis(cfg), solver);
                }
                if (line.hasOption("liveness")) {
                    analyze("liveness", fileName, function, cfg, line,
                            new LivenessAnalysis(cfg), solver);
                }
                if (line.hasOption("udchains")) {
                    log.info("Calculating use-def chains for " + function + " ...");
                    final Map<Node, Set<Definition>> udchains =
                            new UDChains(cfg).chains();
                    if (line.hasOption("cfg") && line.hasOption("annotate")) {
                        log.info("Writing annotated control flow graph"
                                + " to .dot file ...");
                        FileUtils.writeStringToFile(
                                new File(fileName + "." + function
                                        + ".udchains.dot"),
                                    UDChainsDotPrinter.print(cfg, udchains),
                                    "UTF-8");
                        log.info("Created " + fileName + "." + function
                                + ".udchains.dot");
                    }
                }
            }
            log.info("Pretty formatting " + fileName);
            System.out.println(Printer.print(ast));
        } catch (ParserException e) {
            System.err.println("Argument is not a valid .while file.");
            System.exit(1);
        } catch (TypeCheckerException e) {
            System.err.println("Argument is not a typesafe .while file.");
            System.exit(1);
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            printUsage(options);
            System.exit(1);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }
    }
}
