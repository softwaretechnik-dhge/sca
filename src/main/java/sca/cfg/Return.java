package sca.cfg;

public class Return extends Node {
    public Operand value;
    public Return(Operand value) {
        this.value =value;
    }
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitReturn(this);
    }
}
