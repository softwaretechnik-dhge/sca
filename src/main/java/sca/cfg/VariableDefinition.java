package sca.cfg;

public abstract class VariableDefinition extends Node {
    public Variable variable;
    public VariableDefinition(Variable variable) {
        this.variable = variable;
    }
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitVariableDefinition(this);
    }
}
