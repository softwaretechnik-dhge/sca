package sca.cfg;

import java.util.HashMap;

public class Builder
        extends sca.ast.Visitor<IntraproceduralControlFlowGraphImpl> {
    
    public static HashMap<String, IntraproceduralControlFlowGraph> build(sca.ast.Program ast) {
        HashMap<String, IntraproceduralControlFlowGraph> cfgMap = new HashMap<>();
        for (final sca.ast.FunctionDeclaration function : ast.functions) {
            cfgMap.put(function.name.name, function.accept(new Builder()));
        }
        return cfgMap;
    }

    @Override
    public IntraproceduralControlFlowGraphImpl visitNode(sca.ast.Node node) {
        return new IntraproceduralControlFlowGraphImpl();
    }

    @Override
    public IntraproceduralControlFlowGraphImpl visitFunctionDeclaration(
            sca.ast.FunctionDeclaration node) {
        VirtualVariable.counter = 1;
        Node.counter = 0;
        return new IntraproceduralControlFlowGraphImpl(new Entry())
                .append(node.body.accept(this))
                .append(new IntraproceduralControlFlowGraphImpl(new Exit()));
    }
    @Override
    public IntraproceduralControlFlowGraphImpl visitBlock(
            sca.ast.Block node) {
        IntraproceduralControlFlowGraphImpl fragment =
                new IntraproceduralControlFlowGraphImpl();
        for (sca.ast.Statement statement : node.statements) {
            fragment = fragment.append(statement.accept(this));
        }
        return fragment;
    }
    @Override
    public IntraproceduralControlFlowGraphImpl visitIfElseStatement(
            sca.ast.IfElseStatement node) {
        final ExpressionBuilder builder = new ExpressionBuilder();
        final Node condition = new Branch(node.condition.accept(builder));
        final IntraproceduralControlFlowGraphImpl branching =
                builder.graph.append(
                        new IntraproceduralControlFlowGraphImpl(condition));
        IntraproceduralControlFlowGraphImpl fragment =
                node.ifBranch.accept(this);
        fragment = branching.append(fragment);
        if (node.elseBranch != null) {
            IntraproceduralControlFlowGraphImpl other =
                    node.elseBranch.accept(this);
            other = branching.append(other);
            fragment = fragment.merge(other);           
        }
        return fragment;
    }
    @Override
    public IntraproceduralControlFlowGraphImpl visitWhileStatement(
            sca.ast.WhileStatement node) {
        final ExpressionBuilder builder = new ExpressionBuilder();
        final Node condition = new Branch(node.condition.accept(builder));
        final IntraproceduralControlFlowGraphImpl branching =
                builder.graph.append(
                        new IntraproceduralControlFlowGraphImpl(condition));
        IntraproceduralControlFlowGraphImpl loop = node.body.accept(this);
        loop = branching.append(loop.append(branching));
        return loop;
    }
    @Override
    public IntraproceduralControlFlowGraphImpl visitReturnStatement(
            sca.ast.ReturnStatement node) {
        final ExpressionBuilder builder = new ExpressionBuilder();
        final Node stmt = new Return(node.value.accept(builder));
        return builder.graph
                .append(new IntraproceduralControlFlowGraphImpl(stmt));
    }
    @Override
    public IntraproceduralControlFlowGraphImpl visitExpression(
            sca.ast.Expression node) {
        final ExpressionBuilder builder = new ExpressionBuilder();
        if (node instanceof sca.ast.Identifier
                || node instanceof sca.ast.Number) {
            final Node stmt = new SimpleAssignment(
                    new VirtualVariable(),
                    node.accept(builder));
            return new IntraproceduralControlFlowGraphImpl(stmt);
        } else {
            node.accept(builder);
            return builder.graph;
        }
    }
}
