package sca.cfg;

public class Multiply extends BinaryOperation {
    public Multiply(Variable variable, Operand left, Operand right) {
        super(variable, left, right);
    }
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitMultiply(this);
    }
}
