package sca.cfg;

public class Number extends Operand {
    public final int value;
    public Number(int value) {
        this.value = value;
    }
    @Override
    public boolean equals(Object other) {
        return other.getClass() == Number.class
                && ((Number) other).value == value;
    }
    @Override
    public int hashCode() {
        return value;
    }
    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
