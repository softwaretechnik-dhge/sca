package sca.cfg;

public class Greater extends BinaryOperation {
    public Greater(Variable variable, Operand left, Operand right) {
        super(variable, left, right);
    }
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitGreater(this);
    }
}
