package sca.cfg;

public abstract class BinaryOperation extends VariableDefinition {
    public Operand left, right;
    public BinaryOperation(Variable variable, Operand left, Operand right) {
        super(variable);
        this.right = right;
        this.left = left;
    }
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitBinaryOperation(this);
    }
}
