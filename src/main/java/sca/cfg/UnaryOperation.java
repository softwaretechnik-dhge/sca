package sca.cfg;

public class UnaryOperation extends VariableDefinition {
    public Operand operand;
    public UnaryOperation(Variable variable, Operand operand) {
        super(variable);
        this.operand = operand;
    }
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitUnaryOperation(this);
    }
}
