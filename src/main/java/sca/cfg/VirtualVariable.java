package sca.cfg;

public class VirtualVariable extends Variable {
    static int counter = 1;
    public VirtualVariable() {
        super("_t" + counter++);
    }    
}
