package sca.cfg;

public class Equal extends BinaryOperation {
    public Equal(Variable variable, Operand left, Operand right) {
        super(variable, left, right);
    }
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitEqual(this);
    }
}
