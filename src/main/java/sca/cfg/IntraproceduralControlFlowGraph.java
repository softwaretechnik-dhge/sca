package sca.cfg;

public interface IntraproceduralControlFlowGraph {
    public Iterable<Node> nodes();
    public Node entryNode();
    public Node exitNode();
}
