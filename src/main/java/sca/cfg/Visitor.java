package sca.cfg;

public abstract class Visitor<T> {
    public abstract T visitNode(Node node);
    public T visitEntry(Entry node) {
        return visitNode(node);
    }
    public T visitExit(Exit node) {
        return visitNode(node);
    }
    public T visitReturn(Return node) {
        return visitNode(node);
    }
    public T visitBranch(Branch node) {
        return visitNode(node);
    }
    public T visitVariableDefinition(VariableDefinition node) {
        return visitNode(node);
    }
    public T visitSimpleAssignment(SimpleAssignment node) {
        return visitVariableDefinition(node);
    }
    public T visitCall(Call node) {
        return visitVariableDefinition(node);
    }
    public T visitBinaryOperation(BinaryOperation node) {
        return visitVariableDefinition(node);
    }
    public T visitLess(Less node) {
        return visitBinaryOperation(node);
    }
    public T visitGreater(Greater node) {
        return visitBinaryOperation(node);
    }
    public T visitEqual(Equal node) {
        return visitBinaryOperation(node);
    }
    public T visitNotEqual(NotEqual node) {
        return visitBinaryOperation(node);
    }
    public T visitAdd(Add node) {
        return visitBinaryOperation(node);
    }
    public T visitSubtract(Subtract node) {
        return visitBinaryOperation(node);
    }
    public T visitMultiply(Multiply node) {
        return visitBinaryOperation(node);
    }
    public T visitDivide(Divide node) {
        return visitBinaryOperation(node);
    }
    public T visitUnaryOperation(UnaryOperation node) {
        return visitVariableDefinition(node);
    }
    public T visitMinus(Minus node) {
        return visitUnaryOperation(node);
    }
    public T visitNot(Not node) {
        return visitUnaryOperation(node);
    }
}
