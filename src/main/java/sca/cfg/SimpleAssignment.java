package sca.cfg;

public class SimpleAssignment extends VariableDefinition {
    public Operand value;
    public SimpleAssignment(Variable variable, Operand value) {
        super(variable);
        this.value = value;
    }
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitSimpleAssignment(this);
    }
}
