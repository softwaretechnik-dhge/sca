package sca.cfg;

public class Exit extends Node {
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitExit(this);
    }    
}
