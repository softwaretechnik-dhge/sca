package sca.cfg;

public class Less extends BinaryOperation {
    public Less(Variable variable, Operand left, Operand right) {
        super(variable, left, right);
    }
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitLess(this);
    }
}
