package sca.cfg;

public class Not extends UnaryOperation {
    public Not(Variable variable, Operand operand) {
        super(variable, operand);
    }
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitNot(this);
    }
}
