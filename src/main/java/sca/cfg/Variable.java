package sca.cfg;

public class Variable extends Operand {
    public final String name;
    public Variable(String name) {
        this.name = name;
    }
    @Override
    public boolean equals(Object other) {
        return other.getClass() == Variable.class
                && ((Variable) other).name.equals(name);
    }
    @Override
    public int hashCode() {
        return name.hashCode();
    }
    @Override
    public String toString() {
        return name;
    }
}
