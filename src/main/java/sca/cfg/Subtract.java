package sca.cfg;

public class Subtract extends BinaryOperation {
    public Subtract(Variable variable, Operand left, Operand right) {
        super(variable, left, right);
    }
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitSubtract(this);
    }
}
