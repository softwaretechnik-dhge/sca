package sca.cfg;

public class Add extends BinaryOperation {
    public Add(Variable variable, Operand left, Operand right) {
        super(variable, left, right);
    }
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitAdd(this);
    }
}
