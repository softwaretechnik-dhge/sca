package sca.cfg;

public class Minus extends UnaryOperation {
    public Minus(Variable variable, Operand operand) {
        super(variable, operand);
    }
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitMinus(this);
    }
}
