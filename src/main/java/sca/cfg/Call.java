package sca.cfg;

import java.util.List;

public class Call extends VariableDefinition {
    public List<Operand> arguments;
    public Operand target;
    public Call(Variable variable, Operand target, List<Operand> arguments) {
        super(variable);
        this.target = target;
        this.arguments = arguments;
    }
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitCall(this);
    } 
}
