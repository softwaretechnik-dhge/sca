package sca.cfg;

public class Entry extends Node {
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitEntry(this);
    }
}
