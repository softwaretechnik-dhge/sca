package sca.cfg;

public class Branch extends Node {
    public Operand value;
    public Branch(Operand value) {
        this.value =value;
    }
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitBranch(this);
    }
}
