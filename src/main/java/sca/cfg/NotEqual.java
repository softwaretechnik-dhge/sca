package sca.cfg;

public class NotEqual extends BinaryOperation {
    public NotEqual(Variable variable, Operand left, Operand right) {
        super(variable, left, right);
    }
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitNotEqual(this);
    }
}
