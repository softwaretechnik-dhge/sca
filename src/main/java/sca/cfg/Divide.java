package sca.cfg;

public class Divide extends BinaryOperation {
    public Divide(Variable variable, Operand left, Operand right) {
        super(variable, left, right);
    }
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitDivide(this);
    }
}
