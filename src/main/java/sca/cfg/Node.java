package sca.cfg;

import java.util.HashSet;
import java.util.Set;

public abstract class Node {
    static int counter = 0;
    public final Set<Node> predecessors = new HashSet<>(),
            successors = new HashSet<>();
    public final int id;
    public Node() {
        id = counter++;
    }
    @Override
    public boolean equals(Object other) {
        return other.getClass() == Node.class && ((Node) other).id == id;
    }
    @Override
    public int hashCode() {
        return id;
    }
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitNode(this);
    }
}

