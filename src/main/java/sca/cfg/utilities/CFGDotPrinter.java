package sca.cfg.utilities;

import java.util.HashSet;

import sca.cfg.IntraproceduralControlFlowGraph;
import sca.cfg.Node;
import sca.cfg.Visitor;

public class CFGDotPrinter extends Visitor<String> {
    private static final LabelPrinter labelPrinter = new LabelPrinter();
    private final HashSet<Node> visitedNodes = new HashSet<>();

    public static String print(IntraproceduralControlFlowGraph cfg) {
        final StringBuilder sb = new StringBuilder();
        sb.append("digraph {");
        sb.append(new CFGDotPrinter().visitNode(cfg.entryNode()));
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String visitNode(Node node) {
        visitedNodes.add(node);
        final StringBuilder sb = new StringBuilder();
        sb.append(node.id);
        sb.append("[shape=box,label=\"");
        sb.append(node.accept(labelPrinter));
        sb.append("\"];");
        for (Node successor : node.successors) {
            sb.append(node.id);
            sb.append("->");
            sb.append(successor.id);
            sb.append(";");
            if (!visitedNodes.contains(successor)) {
                sb.append(successor.accept(this));
            }
        }
        return sb.toString();
    }
}
