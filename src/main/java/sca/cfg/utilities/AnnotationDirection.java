package sca.cfg.utilities;

public enum AnnotationDirection { FORWARD, BACKWARD }
