package sca.cfg.utilities;

import java.util.HashSet;
import java.util.Map;

import sca.cfg.IntraproceduralControlFlowGraph;
import sca.cfg.Node;
import sca.cfg.Visitor;

public class CFGWithAnnotationsDotPrinter extends Visitor<String> {
    private static final LabelPrinter labelPrinter = new LabelPrinter();
    private final HashSet<Node> visitedNodes = new HashSet<>();
    private final AnnotationDirection annotationDirection;
    private final Map<Node, ?> annotations;

    public CFGWithAnnotationsDotPrinter(AnnotationDirection annotationDirection,
            Map<Node, ?> annotations) {
        this.annotationDirection = annotationDirection;
        this.annotations = annotations;
    }

    public static String print(IntraproceduralControlFlowGraph cfg,
            AnnotationDirection annotationDirection,
            Map<Node, ?> annotations) {
        final StringBuilder sb = new StringBuilder();
        sb.append("digraph {");
        sb.append(
                new CFGWithAnnotationsDotPrinter(annotationDirection,
                        annotations).visitNode(cfg.entryNode()));
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String visitNode(Node node) {
        visitedNodes.add(node);
        final StringBuilder sb = new StringBuilder();
        sb.append(node.id);
        sb.append("[shape=box,label=\"");
        sb.append(node.accept(labelPrinter));
        sb.append("\"];");
        for (Node successor : node.successors) {
            sb.append(node.id);
            sb.append("->");
            sb.append(successor.id);
            if (annotationDirection == AnnotationDirection.FORWARD
                    && annotations.containsKey(node)){
                sb.append("[label=\"");
                sb.append(annotations.get(node));
                sb.append("\",fontcolor=red]");
            } else if (annotationDirection == AnnotationDirection.BACKWARD
                    && annotations.containsKey(successor)){
                sb.append("[label=\"");
                sb.append(annotations.get(successor));
                sb.append("\",fontcolor=red]");
            }
            sb.append(";");
            if (!visitedNodes.contains(successor)) {
                sb.append(successor.accept(this));
            }
        }
        return sb.toString();
    }
}
