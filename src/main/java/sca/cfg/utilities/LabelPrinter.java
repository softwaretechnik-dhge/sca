package sca.cfg.utilities;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import sca.cfg.Add;
import sca.cfg.BinaryOperation;
import sca.cfg.Branch;
import sca.cfg.Call;
import sca.cfg.Divide;
import sca.cfg.Entry;
import sca.cfg.Equal;
import sca.cfg.Exit;
import sca.cfg.Greater;
import sca.cfg.Less;
import sca.cfg.Minus;
import sca.cfg.Multiply;
import sca.cfg.Node;
import sca.cfg.Not;
import sca.cfg.NotEqual;
import sca.cfg.Return;
import sca.cfg.SimpleAssignment;
import sca.cfg.Subtract;
import sca.cfg.UnaryOperation;
import sca.cfg.Visitor;

public class LabelPrinter extends Visitor<String> {
    private static final Map<Class<?>, String> operations = Map.of(
            Less.class, "<",
            Greater.class, ">",
            Equal.class, "==",
            NotEqual.class, "!=",
            Add.class, "+",
            Subtract.class, "-",
            Multiply.class, "*",
            Divide.class, "/",
            Minus.class, "-",
            Not.class, "!");

    @Override
    public String visitNode(Node node) {
        return "???";
    }
    @Override
    public String visitEntry(Entry node) {
        return "entry";
    }
    @Override
    public String visitExit(Exit node) {
        return "exit";
    }
    @Override
    public String visitReturn(Return node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(node.id);
        sb.append(": return ");
        sb.append(node.value);
        return sb.toString();
    }
    @Override
    public String visitBranch(Branch node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(node.id);
        sb.append(": ?(");
        sb.append(node.value);
        sb.append(")");
        return sb.toString();
    }
    @Override
    public String visitSimpleAssignment(SimpleAssignment node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(node.id);
        sb.append(": ");
        sb.append(node.variable);
        sb.append(" = ");
        sb.append(node.value);
        return sb.toString();
    }
    @Override
    public String visitCall(Call node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(node.id);
        sb.append(": ");
        sb.append(node.variable);
        sb.append(" = ");
        sb.append(node.target);
        sb.append("(");
        sb.append(StreamSupport
                .stream(node.arguments.spliterator(), false)
                .map(argument -> argument.toString())
                .collect(Collectors.joining(",")));
        sb.append(")");
        return sb.toString();
    }
    @Override
    public String visitBinaryOperation(BinaryOperation node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(node.id);
        sb.append(": ");
        sb.append(node.variable);
        sb.append(" = ");
        sb.append(node.left);
        sb.append(operations.get(node.getClass()));
        sb.append(node.right);
        return sb.toString();
    }
    @Override
    public String visitUnaryOperation(UnaryOperation node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(node.id);
        sb.append(": ");
        sb.append(node.variable);
        sb.append(" = ");
        sb.append(operations.get(node.getClass()));
        sb.append(node.operand);
        return sb.toString();
    }
}
