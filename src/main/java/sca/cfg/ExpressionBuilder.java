package sca.cfg;

import java.util.LinkedList;

public class ExpressionBuilder extends sca.ast.Visitor<Operand> {
    protected IntraproceduralControlFlowGraphImpl graph =
            new IntraproceduralControlFlowGraphImpl();

    private BinaryOperation processBinaryExpression(Class<?> type,
            Variable variable, Operand left, Operand right) {
        BinaryOperation node = null;
        if (type == sca.ast.Less.class) {
            node = new Less(variable, left, right);
        } else if (type == sca.ast.Greater.class) {
            node = new Greater(variable, left, right);
        } else if (type == sca.ast.Equal.class) {
            node = new Equal(variable, left, right);
        } else if (type == sca.ast.NotEqual.class) {
            node = new NotEqual(variable, left, right);
        } else if (type == sca.ast.Add.class) {
            node = new Add(variable, left, right);
        } else if (type == sca.ast.Subtract.class) {
            node = new Subtract(variable, left, right);
        } else if (type == sca.ast.Multiply.class) {
            node = new Multiply(variable, left, right);
        } else if (type == sca.ast.Divide.class) {
            node = new Divide(variable, left, right);
        }
        return node;
    }
    private UnaryOperation processUnaryExpression(Class<?> type, Variable variable,
            Operand operand) {
        UnaryOperation node = null;
        if (type == sca.ast.Minus.class) {
            node = new Minus(variable, operand);
        } else if (type == sca.ast.Not.class) {
            node = new Not(variable, operand);
        }
        return node;
    }

    @Override
    public Operand visitNode(sca.ast.Node node) {
        return new VirtualVariable();
    }

    @Override
    public Operand visitNumber(sca.ast.Number node) {
        return new Number(node.value);
    }
    @Override
    public Operand visitAssignment(sca.ast.Assignment node) {
        final Variable variable;
        if (node.target instanceof sca.ast.Identifier) {
            variable = new Variable(((sca.ast.Identifier) node.target).name);
        } else {
            variable = new VirtualVariable();
        }
        Node stmt;
        if (node.source instanceof sca.ast.BinaryExpression) {
            sca.ast.BinaryExpression source =
                    (sca.ast.BinaryExpression) node.source;
            stmt = processBinaryExpression(
                    source.getClass(),
                    variable,
                    source.left.accept(this),
                    source.right.accept(this));
        } else if (node.source instanceof sca.ast.UnaryExpression) {
            sca.ast.UnaryExpression source =
                    (sca.ast.UnaryExpression) node.source;
            stmt = processUnaryExpression(
                    source.getClass(),
                    variable,
                    source.operand.accept(this));
        }  else if (node.source instanceof sca.ast.Call) {
            sca.ast.Call source = (sca.ast.Call) node.source;
            LinkedList<Operand> arguments = new LinkedList<>();
            for (sca.ast.Node argument : source.arguments.arguments) {
                arguments.add(argument.accept(this));
            }
            final Operand receiver = source.callee.accept(this);
            stmt = new Call(variable, receiver, arguments);
        } else {
            stmt = new SimpleAssignment(variable, node.source.accept(this));
        }
        graph = graph.append(new IntraproceduralControlFlowGraphImpl(stmt));
        return variable;
    }
    @Override
    public Operand visitBinaryExpression(sca.ast.BinaryExpression node) {
        final Variable variable = new VirtualVariable();
        final Operand left = node.left.accept(this);
        final Operand right = node.right.accept(this);
        final Node stmt = processBinaryExpression(
                node.getClass(),
                variable,
                left,
                right);
        graph = graph.append(new IntraproceduralControlFlowGraphImpl(stmt));
        return variable;
    }
    @Override
    public Operand visitUnaryExpression(sca.ast.UnaryExpression node) {
        final Variable variable = new VirtualVariable();
        final Operand operand = node.operand.accept(this);
        final Node stmt = processUnaryExpression(
                node.getClass(),
                variable,
                operand);
        graph = graph.append(new IntraproceduralControlFlowGraphImpl(stmt));
        return variable;
    }
    @Override
    public Operand visitCall(sca.ast.Call node) {
        final Variable variable = new VirtualVariable();
        LinkedList<Operand> arguments = new LinkedList<>();
        for (sca.ast.Node argument : node.arguments.arguments) {
            arguments.add(argument.accept(this));
        }
        final Operand receiver = node.callee.accept(this);
        final Node stmt = new Call(variable, receiver, arguments);
        graph = graph.append(new IntraproceduralControlFlowGraphImpl(stmt));
        return variable;
    }
    @Override
    public Operand visitIdentifier(sca.ast.Identifier node) {
        return new Variable(node.name);
    }
}
