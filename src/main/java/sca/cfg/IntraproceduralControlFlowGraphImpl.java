package sca.cfg;

import java.util.HashSet;
import java.util.Set;

public class IntraproceduralControlFlowGraphImpl
        implements IntraproceduralControlFlowGraph {
    private final Set<Node> entryNodes, exitNodes;
    private Set<Node> nodes;
    private Set<Node> collect(Node node, Set<Node> visited) {
        if (!visited.contains(node)) {
            visited.add(node);
            for (Node successor : node.successors) {
              collect(successor, visited);
            }
        }
        return visited;
    }

    public IntraproceduralControlFlowGraphImpl(Set<Node> entryNodes,
            Set<Node> exitNodes) {
        this.entryNodes = new HashSet<>(entryNodes);
        this.exitNodes = new HashSet<>(exitNodes);
    }
    public IntraproceduralControlFlowGraphImpl(Node node) {
        this(new HashSet<>() {{add(node);}}, new HashSet<>() {{add(node);}});
    }
    public IntraproceduralControlFlowGraphImpl() {
        this(new HashSet<>(), new HashSet<>());
    }

    @Override
    public Node entryNode() {
        return entryNodes.isEmpty() ? null : entryNodes.iterator().next();
    }
    @Override
    public Node exitNode() {
        return exitNodes.isEmpty() ? null : exitNodes.iterator().next();
    }
    @Override
    public Iterable<Node> nodes() {
        if (nodes == null) {
            nodes = new HashSet<>();
            for (Node node : entryNodes) {
                nodes.addAll(collect(node, new HashSet<>()));
            }
        }
        return nodes;
    }
    public IntraproceduralControlFlowGraphImpl merge(
            IntraproceduralControlFlowGraphImpl other) {
        HashSet<Node> entryNodes = new HashSet<>(),
                exitNodes = new HashSet<>();
        entryNodes.addAll(other.entryNodes);
        entryNodes.addAll(this.entryNodes);
        exitNodes.addAll(other.exitNodes);
        exitNodes.addAll(this.exitNodes);
        return new IntraproceduralControlFlowGraphImpl(entryNodes, exitNodes);
    }
    public IntraproceduralControlFlowGraphImpl append(
            IntraproceduralControlFlowGraphImpl other) {
        if (other.entryNodes.isEmpty() && other.exitNodes.isEmpty()) {
            return this;
        } else if (entryNodes.isEmpty() && exitNodes.isEmpty()) {
            return other;
        } else {
            for (Node node : other.entryNodes) {
                node.predecessors.addAll(exitNodes);
            }
            for (Node node : exitNodes) {
                node.successors.addAll(other.entryNodes);
            }
            return new IntraproceduralControlFlowGraphImpl(entryNodes,
                    other.exitNodes);
        }
    }
}
