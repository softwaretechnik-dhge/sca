package sca.types;

class IntegerType extends Term {
    private static final IntegerType instance = new IntegerType();
    public static IntegerType getInstance() {
        return instance;
    }
    private IntegerType() {}
    @Override
    public String toString() {
        return "int";
    }
}
