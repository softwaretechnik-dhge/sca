package sca.types;

import sca.ast.utilities.Printer;
import sca.ast.Expression;

public class ExpressionVariable extends TypeVariable {
    private static final Printer printer = new Printer();
    private final Expression node;
    public ExpressionVariable(Expression node) {
        this.node = node;
    }
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[[");
        sb.append(node.accept(printer));
        sb.append("]]");
        return sb.toString();
    }
}
  
