package sca.types;

import java.util.stream.StreamSupport;
import java.util.stream.Collectors;
import java.util.List;

public class FunctionType extends Term {
    public final List<Term> parameterTypes;
    public final Term returnType;
    public FunctionType(List<Term> parameterTypes, Term returnType) {
        this.parameterTypes = parameterTypes;
        this.returnType = returnType;
    }
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("(");
        sb.append(StreamSupport
                .stream(parameterTypes.spliterator(), false)
                .map(Object::toString)
                .collect(Collectors.joining(",")));
        sb.append(")->");
        sb.append(returnType);
        return sb.toString();
    }
}
