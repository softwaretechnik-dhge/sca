package sca.types;

public class TypeVariable extends Term {
    private static int counter = 0;
    private final int id ;
    public TypeVariable() {
        id = counter++;
    }
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("\u03b1");
        sb.append(id);
        return sb.toString();
    }
}
