package sca.types;

import java.util.HashMap;

public class UnionFind<T> {
    private final HashMap<T, T> parent = new HashMap<>();
    public void makeSet(T x) {
        parent.putIfAbsent(x, x);
    }
    public T find(T x) {
        if (!x.equals(parent.get(x))) {
            parent.put(x, find(parent.get(x)));
        }
        return parent.get(x);
    }
    public void union(T x, T y) {
        parent.put(x, y);
    }
  }
