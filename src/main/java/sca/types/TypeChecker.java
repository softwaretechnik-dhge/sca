package sca.types;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import sca.ast.Assignment;
import sca.ast.BinaryExpression;
import sca.ast.Block;
import sca.ast.Call;
import sca.ast.Equal;
import sca.ast.Expression;
import sca.ast.FunctionDeclaration;
import sca.ast.Identifier;
import sca.ast.IfElseStatement;
import sca.ast.Node;
import sca.ast.NotEqual;
import sca.ast.Number;
import sca.ast.Program;
import sca.ast.ReturnStatement;
import sca.ast.Statement;
import sca.ast.UnaryExpression;
import sca.ast.VisitorWithException;
import sca.ast.WhileStatement;

public class TypeChecker extends VisitorWithException {
    private static final Logger log = LogManager.getLogger(TypeChecker.class);

    private HashMap<Object, TypeVariable> typeVariables;
    private FunctionDeclaration currentFunction; // maybe null
    private UnionFindSolver solver;
  
    private TypeVariable typeVariable(Expression node) {
        return typeVariables.computeIfAbsent(
                node instanceof Identifier ? ((Identifier) node).name : node,
                object -> new ExpressionVariable(node));
    }
    private void unify(Term x, Term y) throws Exception {
        log.debug("Typechecking: new type constraint " + x + " = " + y);
        solver.unify(x, y);
    }
  
    public void typecheck(Program ast) throws Exception {
        typeVariables = new HashMap<Object, TypeVariable>();
        solver = new UnionFindSolver();
        // predefined functions: [[scan]] = ()->int /\ [[print]] = (int)->int
        // main function: [[main]] = ()->int
        unify(typeVariable(new Identifier("main")),
                new FunctionType(List.of(), IntegerType.getInstance()));
        unify(typeVariable(new Identifier("scan")),
                new FunctionType(List.of(), IntegerType.getInstance()));
        unify(typeVariable(new Identifier("print")),
                new FunctionType(List.of(IntegerType.getInstance()),
                        IntegerType.getInstance()));
        visitProgram(ast);
    }

    // TODO: add type annotations, dynamic type, and gradual typing
    // TODO: support non-disjunct variable names
    // TODO: add parametric polymorphism

    @Override
    public void visitNode(Node node) {}
    @Override
    public void visitProgram(Program node) throws Exception {
        for (FunctionDeclaration function : node.functions) {
            function.accept(this);
        }
    }
    @Override
    public void visitBlock(Block node) throws Exception {
        for (Statement statement : node.statements) {
            statement.accept(this);
        }
    }
    // [[name]] = (...,[[parameter]],...) -> [[currentFunction]]
    @Override
    public void visitFunctionDeclaration(FunctionDeclaration node)
            throws Exception {
        currentFunction = node;
        node.body.accept(this);
        final LinkedList<Term> parameterTypes = new LinkedList<>();
        for (Identifier parameter : node.parameters.parameters) {
            parameterTypes.add(typeVariable(parameter));
        }
        unify(
            typeVariable(node.name),
            new FunctionType(
                    parameterTypes,
                    typeVariables.computeIfAbsent(currentFunction,
                            currentFunction -> new TypeVariable())));
    }
    // [[condition]] = int
    @Override
    public void visitIfElseStatement(IfElseStatement node) throws Exception {
        node.condition.accept(this);
        node.ifBranch.accept(this);
        if (node.elseBranch != null) {
            node.elseBranch.accept(this);
        }
        unify(typeVariable(node.condition), IntegerType.getInstance());
    }
    // [[condition]] = int
    @Override
    public void visitWhileStatement(WhileStatement node) throws Exception {
        node.condition.accept(this);
        node.body.accept(this);
        unify(typeVariable(node.condition), IntegerType.getInstance());
    }
    @Override
    // [[target = source]] = [[source]]
    // [[target]] = [[source]]
    public void visitAssignment(Assignment node) throws Exception {
        node.source.accept(this);
        node.target.accept(this);
        unify(typeVariable(node), typeVariable(node.source));
        unify(typeVariable(node.target), typeVariable(node.source));
    } 
    // [[value]] = [[currentFunction]]
    @Override
    public void visitReturnStatement(ReturnStatement node) throws Exception {
        node.value.accept(this);
        unify(typeVariable(node.value),
                typeVariables.computeIfAbsent(currentFunction,
                        currentFunction -> new TypeVariable()));
    }
    // [[left + right]] = int
    // [[left - right]] = int
    // [[left * right]] = int
    // [[left / right]] = int
    // [[left > right]] = int
    // [[left < right]] = int
    // [[left]] = [[right]]
    // [[left]] = int
    @Override
    public void visitBinaryExpression(BinaryExpression node)
            throws Exception {
        node.left.accept(this);
        node.right.accept(this);
        unify(typeVariable(node.left), typeVariable(node.right));
        unify(typeVariable(node.left), IntegerType.getInstance());
        unify(typeVariable(node), IntegerType.getInstance());
    }
    // [[left == right]] = int
    // [[left]] = [[right]]
    @Override
    public void visitEqual(Equal node) throws Exception {
        node.left.accept(this);
        node.right.accept(this);
        unify(typeVariable(node.left), typeVariable(node.right));
        unify(typeVariable(node), IntegerType.getInstance());
    }
    // [[left != right]] = int
    // [[left]] = [[right]]
    @Override
    public void visitNotEqual(NotEqual node) throws Exception {
        node.left.accept(this);
        node.right.accept(this);
        unify(typeVariable(node.left), typeVariable(node.right));
        unify(typeVariable(node), IntegerType.getInstance());
    }
    // [[-operand]] = int
    // [[!operand]] = int
    // [[operand]] = int
    @Override
    public void visitUnaryExpression(UnaryExpression node) throws Exception {
        node.operand.accept(this);
        unify(typeVariable(node.operand), IntegerType.getInstance());
        unify(typeVariable(node), IntegerType.getInstance());
    }
    // [[callee]] = (...,[[argument]],...) -> [[callee(..., argument, ...)]]
    @Override
    public void visitCall(Call node) throws Exception {
        for (Expression argument : node.arguments.arguments) {
            argument.accept(this);
        }
        node.callee.accept(this);
        LinkedList<Term> argumentTypes = new LinkedList<>();
        for (Expression argument : node.arguments.arguments) {
            argumentTypes.add(typeVariable(argument));
        }
        unify(
            typeVariable(node.callee),
            new FunctionType(argumentTypes, typeVariable(node)));
    } 
    // [[number]] = int
    @Override
    public void visitNumber(Number node) throws Exception {
        unify(typeVariable(node), IntegerType.getInstance());
    }
}
