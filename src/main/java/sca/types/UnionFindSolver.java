package sca.types;

public class UnionFindSolver extends UnionFind<Term> {
    public void unify(Term x, Term y) throws TypeCheckerException {
        makeSet(x);
        makeSet(y);
        x = find(x);
        y = find(y);
        if (!x.equals(y)) {
            if (x instanceof TypeVariable && y instanceof TypeVariable) {
                union(x, y);
            } else if (x instanceof TypeVariable) {
                union(x, y);
            } else if (y instanceof TypeVariable) {
                union(y, x);
            } else if (x instanceof FunctionType &&
                    y instanceof FunctionType) {
                FunctionType xft = (FunctionType) x, yft = (FunctionType) y;
                if (xft.parameterTypes.size() == yft.parameterTypes.size()) {
                    union(x, y);
                    for (var i = 0; i < xft.parameterTypes.size(); i++) {
                        unify(xft.parameterTypes.get(i),
                                yft.parameterTypes.get(i));
                    }
                    unify(xft.returnType, yft.returnType);
                } else {
                    throw new TypeCheckerException();
                }
            } else {
                throw new TypeCheckerException();
            }
        }
    }
}
