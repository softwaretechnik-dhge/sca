package sca.ast;

public class Call extends Expression {
    public final Arguments arguments;
    public final Expression callee;
    public Call(Arguments arguments, Expression callee) {
        this.arguments = arguments;
        this.callee = callee;
    }
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitCall(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitCall(this);
    }
}
