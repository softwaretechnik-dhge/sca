package sca.ast;

public abstract class Statement extends Node {
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitStatement(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitStatement(this);
    }
}
