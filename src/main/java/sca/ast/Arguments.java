package sca.ast;

public class Arguments extends Node {
    public final Iterable<Expression> arguments;
    public Arguments(Iterable<Expression> arguments) {
        this.arguments = arguments;
    }
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitArguments(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitArguments(this);
    }
}
