package sca.ast;

public class ReturnStatement extends Statement {
    public final Expression value;
    public ReturnStatement(Expression value) {
        this.value = value;
    }
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitReturnStatement(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitReturnStatement(this);
    }
}
