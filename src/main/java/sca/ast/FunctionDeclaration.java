package sca.ast;

public class FunctionDeclaration extends Node {
    public final Parameters parameters;
    public final Identifier name;
    public final Block body;
    public FunctionDeclaration(Identifier name, Parameters parameters, Block body) {
        this.name = name;
        this.parameters = parameters;
        this.body = body;
    }
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitFunctionDeclaration(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitFunctionDeclaration(this);
    }
  }