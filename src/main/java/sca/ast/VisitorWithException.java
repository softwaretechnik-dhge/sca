package sca.ast;

public abstract class VisitorWithException {
    public abstract void visitNode(Node node) throws Exception;
    public void visitProgram(Program node) throws Exception {
        visitNode(node);
    }
    public void visitFunctionDeclaration(FunctionDeclaration node)
            throws Exception {
        visitNode(node);
    }
    public void visitStatement(Statement node) throws Exception {
        visitNode(node);
    }
    public void visitBlock(Block node) throws Exception {
        visitStatement(node);
    }
    public void visitIfElseStatement(IfElseStatement node) throws Exception {
        visitStatement(node);
    }
    public void visitWhileStatement(WhileStatement node) throws Exception {
        visitStatement(node);
    }
    public void visitReturnStatement(ReturnStatement node) throws Exception {
        visitStatement(node);
    }
    public void visitExpression(Expression node) throws Exception {
        visitStatement(node);
    }
    public void visitNumber(Number node) throws Exception {
        visitExpression(node);
    }
    public void visitAssignment(Assignment node) throws Exception {
        visitExpression(node);
    }
    public void visitBinaryExpression(BinaryExpression node)
            throws Exception {
        visitExpression(node);
    }
    public void visitLess(Less node) throws Exception {
        visitBinaryExpression(node);
    }
    public void visitGreater(Greater node) throws Exception {
        visitBinaryExpression(node);
    }
    public void visitEqual(Equal node) throws Exception {
        visitBinaryExpression(node);
    }
    public void visitNotEqual(NotEqual node) throws Exception {
        visitBinaryExpression(node);
    }
    public void visitAdd(Add node) throws Exception {
        visitBinaryExpression(node);
    }
    public void visitSubtract(Subtract node) throws Exception {
        visitBinaryExpression(node);
    }
    public void visitMultiply(Multiply node) throws Exception {
        visitBinaryExpression(node);
    }
    public void visitDivide(Divide node) throws Exception {
        visitBinaryExpression(node);
    }
    public void visitUnaryExpression(UnaryExpression node) throws Exception {
        visitExpression(node);
    }
    public void visitMinus(Minus node) throws Exception {
        visitUnaryExpression(node);
    }
    public void visitNot(Not node) throws Exception {
        visitUnaryExpression(node);
    }
    public void visitCall(Call node) throws Exception {
        visitExpression(node);
    }
    public void visitAssignableExpression(AssignableExpression node)
            throws Exception {
        visitExpression(node);
    }
    public void visitIdentifier(Identifier node) throws Exception {
        visitAssignableExpression(node);
    }
    public void visitParameters(Parameters node) throws Exception {
        visitNode(node);
    }
    public void visitArguments(Arguments node) throws Exception {
        visitNode(node);
    }
}
