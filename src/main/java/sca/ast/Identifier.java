package sca.ast;

public class Identifier extends AssignableExpression {
    public final String name;
    public Identifier(String name) {
        this.name = name;
    }
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitIdentifier(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitIdentifier(this);
    }
}
