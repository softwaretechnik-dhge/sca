package sca.ast;

public class IfElseStatement extends Statement {
    public final Statement elseBranch; // maybe null
    public final Statement ifBranch;
    public final Expression condition;
    public IfElseStatement(Statement ifBranch, Statement elseBranch,
            Expression condition) {
        this.elseBranch = elseBranch;
        this.ifBranch = ifBranch;
        this.condition = condition;
    }
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitIfElseStatement(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitIfElseStatement(this);
    }
}
