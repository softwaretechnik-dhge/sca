package sca.ast;

public abstract class Visitor<T> {
    public abstract T visitNode(Node node);
    public T visitProgram(Program node) {
        return visitNode(node);
    }
    public T visitFunctionDeclaration(FunctionDeclaration node) {
        return visitNode(node);
    }
    public T visitStatement(Statement node) {
        return visitNode(node);
    }
    public T visitBlock(Block node) {
        return visitStatement(node);
    }
    public T visitIfElseStatement(IfElseStatement node) {
        return visitStatement(node);
    }
    public T visitWhileStatement(WhileStatement node) {
        return visitStatement(node);
    }
    public T visitReturnStatement(ReturnStatement node) {
        return visitStatement(node);
    }
    public T visitExpression(Expression node) {
        return visitStatement(node);
    }
    public T visitNumber(Number node) {
        return visitExpression(node);
    }
    public T visitAssignment(Assignment node) {
        return visitExpression(node);
    }
    public T visitBinaryExpression(BinaryExpression node) {
        return visitExpression(node);
    }
    public T visitLess(Less node) {
        return visitBinaryExpression(node);
    }
    public T visitGreater(Greater node) {
        return visitBinaryExpression(node);
    }
    public T visitEqual(Equal node) {
        return visitBinaryExpression(node);
    }
    public T visitNotEqual(NotEqual node) {
        return visitBinaryExpression(node);
    }
    public T visitAdd(Add node) {
        return visitBinaryExpression(node);
    }
    public T visitSubtract(Subtract node) {
        return visitBinaryExpression(node);
    }
    public T visitMultiply(Multiply node) {
        return visitBinaryExpression(node);
    }
    public T visitDivide(Divide node) {
        return visitBinaryExpression(node);
    }
    public T visitUnaryExpression(UnaryExpression node) {
        return visitExpression(node);
    }
    public T visitMinus(Minus node) {
        return visitUnaryExpression(node);
    }
    public T visitNot(Not node) {
        return visitUnaryExpression(node);
    }
    public T visitCall(Call node) {
        return visitExpression(node);
    }
    public T visitAssignableExpression(AssignableExpression node) {
        return visitExpression(node);
    }
    public T visitIdentifier(Identifier node) {
        return visitAssignableExpression(node);
    }
    public T visitParameters(Parameters node) {
        return visitNode(node);
    }
    public T visitArguments(Arguments node) {
        return visitNode(node);
    }
}