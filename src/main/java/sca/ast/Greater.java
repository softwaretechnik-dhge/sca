package sca.ast;

public class Greater extends BinaryExpression {
    public Greater(Expression left, Expression right) {
        super(left, right, ">");
    }
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitGreater(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitGreater(this);
    }
}
