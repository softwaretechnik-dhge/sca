package sca.ast;

public abstract class BinaryExpression extends Expression {
    public final Expression left, right;
    public final String operator;
    public BinaryExpression(Expression left,
            Expression right, String operator) {
        this.left = left;
        this.right = right;
        this.operator = operator;
    }
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitBinaryExpression(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitBinaryExpression(this);
    }
}
