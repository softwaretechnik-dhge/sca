package sca.ast;

public abstract class AssignableExpression extends Expression {
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitAssignableExpression(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitAssignableExpression(this);
    }
}
