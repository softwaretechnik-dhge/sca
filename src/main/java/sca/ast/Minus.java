package sca.ast;

public class Minus extends UnaryExpression {
    public Minus(Expression operand) {
        super(operand, "-");
    }
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitMinus(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitMinus(this);
    }
}
