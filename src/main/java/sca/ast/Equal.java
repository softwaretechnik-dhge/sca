package sca.ast;

public class Equal extends BinaryExpression {
    public Equal(Expression left, Expression right) {
        super(left, right, "==");
    }
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitEqual(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitEqual(this);
    }
}
