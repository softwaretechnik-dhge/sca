package sca.ast;

public abstract class Node {
    private static int counter = 0;
    public final int id;
    public Node() {
        id = counter++;
    }
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitNode(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitNode(this);
    }
}
