package sca.ast;

public class Number extends Expression {
    public final int value;
    public Number(int value) {
        this.value = value;
    }
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitNumber(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitNumber(this);
    }
}
