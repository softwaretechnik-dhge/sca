package sca.ast;

public class Multiply extends BinaryExpression {
    public Multiply(Expression left, Expression right) {
        super(left, right, "*");
    }
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitMultiply(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitMultiply(this);
    }
}
