package sca.ast;

public class Divide extends BinaryExpression {
    public Divide(Expression left, Expression right) {
        super(left, right, "/");
    }
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitDivide(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitDivide(this);
    }
}
