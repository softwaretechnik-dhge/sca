package sca.ast;

public abstract class Expression extends Statement {
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitExpression(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitExpression(this);
    }
}
