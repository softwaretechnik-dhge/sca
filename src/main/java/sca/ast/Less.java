package sca.ast;

public class Less extends BinaryExpression {
    public Less(Expression left, Expression right) {
        super(left, right, "<");
    }
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitLess(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitLess(this);
    }
}
