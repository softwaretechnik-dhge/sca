package sca.ast;

public class Block extends Statement {
    public final Iterable<Statement> statements;
    public Block(Iterable<Statement> statements) {
        this.statements = statements;
    }
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitBlock(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitBlock(this);
    }
}
