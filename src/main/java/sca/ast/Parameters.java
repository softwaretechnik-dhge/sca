package sca.ast;

public class Parameters extends Node {
    public final Iterable<Identifier> parameters;
    public Parameters(Iterable<Identifier> parameters) {
        this.parameters = parameters;
    }
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitParameters(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitParameters(this);
    }
}
