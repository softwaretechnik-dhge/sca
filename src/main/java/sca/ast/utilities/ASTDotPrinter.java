package sca.ast.utilities;

import sca.ast.Arguments;
import sca.ast.Assignment;
import sca.ast.BinaryExpression;
import sca.ast.Block;
import sca.ast.Call;
import sca.ast.Expression;
import sca.ast.FunctionDeclaration;
import sca.ast.Identifier;
import sca.ast.IfElseStatement;
import sca.ast.Node;
import sca.ast.Number;
import sca.ast.Parameters;
import sca.ast.Program;
import sca.ast.ReturnStatement;
import sca.ast.Statement;
import sca.ast.UnaryExpression;
import sca.ast.Visitor;
import sca.ast.WhileStatement;

public class ASTDotPrinter extends Visitor<String> {
    public static String print(Program ast) {
        final StringBuilder sb = new StringBuilder();
        sb.append("digraph {");
        sb.append(new ASTDotPrinter().visitProgram(ast));
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String visitNode(Node node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(node.id);
        sb.append("[shape=box,label=\"");
        sb.append(node.getClass().getSimpleName());
        sb.append("\"];");
        return sb.toString();
    }
    @Override
    public String visitProgram(Program node) {
        final StringBuilder sb = new StringBuilder();
        for (FunctionDeclaration function : node.functions) {
            sb.append(function.accept(this));
            sb.append(node.id);
            sb.append("->");
            sb.append(function.id);
            sb.append(";");
        }
        sb.append(visitNode(node));
        return sb.toString();
    }
    @Override
    public String visitFunctionDeclaration(FunctionDeclaration node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(node.name.accept(this));
        sb.append(node.parameters.accept(this));
        sb.append(node.body.accept(this));
        sb.append(visitNode(node));
        sb.append(node.id);
        sb.append("->");
        sb.append(node.name.id);
        sb.append(";");
        sb.append(node.id);
        sb.append("->");
        sb.append(node.parameters.id);
        sb.append(";");
        sb.append(node.id);
        sb.append("->");
        sb.append(node.body.id);
        sb.append(";");
        return sb.toString();
    }
    @Override
    public String visitBlock(Block node) {
        final StringBuilder sb = new StringBuilder();
        for (Statement statement : node.statements) {
            sb.append(statement.accept(this));
            sb.append(node.id);
            sb.append("->");
            sb.append(statement.id);
            sb.append(";");
        }
        sb.append(visitNode(node));
        return sb.toString();
    }
    @Override
    public String visitIfElseStatement(IfElseStatement node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(node.condition.accept(this));
        sb.append(node.ifBranch.accept(this));
        if (node.elseBranch != null) {
            sb.append(node.elseBranch.accept(this));
        }
        sb.append(visitNode(node));
        sb.append(node.id);
        sb.append("->");
        sb.append(node.condition.id);
        sb.append(";");
        sb.append(node.id);
        sb.append("->");
        sb.append(node.ifBranch.id);
        sb.append(";");
        if (node.elseBranch != null) {
            sb.append(node.id);
            sb.append("->");
            sb.append(node.elseBranch.id);
            sb.append(";");
        }
        return sb.toString();
    }
    @Override
    public String visitWhileStatement(WhileStatement node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(node.condition.accept(this));
        sb.append(node.body.accept(this));
        sb.append(visitNode(node));
        sb.append(node.id);
        sb.append("->");
        sb.append(node.condition.id);
        sb.append(";");
        sb.append(node.id);
        sb.append("->");
        sb.append(node.body.id);
        sb.append(";");
        return sb.toString();
    }
    @Override
    public String visitReturnStatement(ReturnStatement node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(node.value.accept(this));
        sb.append(visitNode(node));
        sb.append(node.id);
        sb.append("->");
        sb.append(node.value.id);
        sb.append(";");
        return sb.toString();
    }
    @Override
    public String visitNumber(Number node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(node.id);
        sb.append("[shape=box,label=\"Number\\n\\\"");
        sb.append(node.value);
        sb.append("\\\"\"];");
        return sb.toString();
    }
    @Override
    public String visitAssignment(Assignment node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(node.target.accept(this));
        sb.append(node.source.accept(this));
        sb.append(visitNode(node));
        sb.append(node.id);
        sb.append("->");
        sb.append(node.source.id);
        sb.append(";");
        sb.append(node.id);
        sb.append("->");
        sb.append(node.target.id);
        sb.append(";");
        return sb.toString();
    }
    @Override
    public String visitBinaryExpression(BinaryExpression node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(node.left.accept(this));
        sb.append(node.right.accept(this));
        sb.append(visitNode(node));
        sb.append(node.id);
        sb.append("->");
        sb.append(node.left.id);
        sb.append(";");
        sb.append(node.id);
        sb.append("->");
        sb.append(node.right.id);
        sb.append(";");
        return sb.toString();
    }
    @Override
    public String visitUnaryExpression(UnaryExpression node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(node.operand.accept(this));
        sb.append(visitNode(node));
        sb.append(node.id);
        sb.append("->");
        sb.append(node.operand.id);
        sb.append(";");
        return sb.toString();
    }
    @Override
    public String visitCall(Call node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(node.callee.accept(this));
        sb.append(node.arguments.accept(this));
        sb.append(visitNode(node));
        sb.append(node.id);
        sb.append("->");
        sb.append(node.callee.id);
        sb.append(";");
        sb.append(node.id);
        sb.append("->");
        sb.append(node.arguments.id);
        sb.append(";");
        return sb.toString();
    }
    @Override
    public String visitIdentifier(Identifier node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(node.id);
        sb.append("[shape=box,label=\"Identifier\\n\\\"");
        sb.append(node.name);
        sb.append("\\\"\"];");
        return sb.toString();
    }
    @Override
    public String visitParameters(Parameters node) {
        final StringBuilder sb = new StringBuilder();
        for (Identifier identifier : node.parameters) {
            sb.append(identifier.accept(this));
            sb.append(node.id);
            sb.append("->");
            sb.append(identifier.id);
            sb.append(";");
        }
        sb.append(visitNode(node));
        return sb.toString();
    }
    @Override
    public String visitArguments(Arguments node) {
        final StringBuilder sb = new StringBuilder();
        for (Expression expression : node.arguments) {
            sb.append(expression.accept(this));
            sb.append(node.id);
            sb.append("->");
            sb.append(expression.id);
            sb.append(";");
        }
        sb.append(visitNode(node));
        return sb.toString();
    }
}
