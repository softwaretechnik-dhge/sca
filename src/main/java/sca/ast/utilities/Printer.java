package sca.ast.utilities;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import sca.ast.Arguments;
import sca.ast.Assignment;
import sca.ast.BinaryExpression;
import sca.ast.Block;
import sca.ast.Call;
import sca.ast.Expression;
import sca.ast.FunctionDeclaration;
import sca.ast.Identifier;
import sca.ast.IfElseStatement;
import sca.ast.Node;
import sca.ast.Number;
import sca.ast.Parameters;
import sca.ast.Program;
import sca.ast.ReturnStatement;
import sca.ast.Statement;
import sca.ast.UnaryExpression;
import sca.ast.Visitor;
import sca.ast.WhileStatement;

public class Printer extends Visitor<String> {
    public static String print(Program ast) {
        return new Printer().visitProgram(ast);
    }

    private int indent = 0;

    private String indentation() {
        return "  ".repeat(indent);
    }
    private String statement(Statement statement) {
        final StringBuilder sb = new StringBuilder();
        sb.append(statement instanceof Block ? "" : indentation());
        sb.append(statement.accept(this));
        sb.append(statement instanceof Expression ? ";" : "");
        return sb.toString();
    }

    @Override
    public String visitNode(Node node) {
        return "???";
    }
    @Override
    public String visitProgram(Program node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(StreamSupport
                .stream(node.functions.spliterator(), false)
                .map(function -> function.accept(this))
                .collect(Collectors.joining("\n")));
        return sb.toString();
    }
    @Override
    public String visitFunctionDeclaration(FunctionDeclaration node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(node.name.accept(this));
        sb.append("(");
        sb.append(node.parameters.accept(this));
        sb.append(") {\n");
        indent++;
        sb.append(statement(node.body));
        indent--;
        sb.append("\n}");
        return sb.toString();
    }
    @Override
    public String visitBlock(Block node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(StreamSupport
                .stream(node.statements.spliterator(), false)
                .map(statement -> statement(statement))
                .collect(Collectors.joining("\n")));
        return sb.toString();
    }
    @Override
    public String visitIfElseStatement(IfElseStatement node) {
        final StringBuilder sb = new StringBuilder();
        sb.append("if (");
        sb.append(node.condition.accept(this));
        sb.append(") {\n");
        indent++;
        sb.append(statement(node.ifBranch));
        indent--;
        sb.append("\n");
        sb.append(indentation());
        sb.append("}");
        if (node.elseBranch != null) {
            sb.append(" else {\n");
            indent++;
            sb.append(statement(node.elseBranch));
            indent--;
            sb.append("\n");
            sb.append(indentation());
            sb.append("}");
        }
        return sb.toString();        
    }
    @Override
    public String visitWhileStatement(WhileStatement node) {
        final StringBuilder sb = new StringBuilder();
        sb.append("while (");
        sb.append(node.condition.accept(this));
        sb.append(") {\n");
        indent++;
        sb.append(statement(node.body));
        indent--;
        sb.append("\n");
        sb.append(indentation());
        sb.append("}");
        return sb.toString();
    }
    @Override
    public String visitReturnStatement(ReturnStatement node) {
        final StringBuilder sb = new StringBuilder();
        sb.append("return ");
        sb.append(node.value.accept(this));
        sb.append(";");
        return sb.toString();
    }
    @Override
    public String visitNumber(Number node) {
        return String.valueOf(node.value);
    }
    @Override
    public String visitAssignment(Assignment node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(node.target.accept(this));
        sb.append(" = ");
        sb.append(node.source.accept(this));
        return sb.toString();
    }
    @Override
    public String visitBinaryExpression(BinaryExpression node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(node.left instanceof BinaryExpression ? "(" : "");
        sb.append(node.left.accept(this));
        sb.append(node.left instanceof BinaryExpression ? ")" : "");
        sb.append(node.operator);
        sb.append(node.right instanceof BinaryExpression ? "(" : "");
        sb.append(node.right.accept(this));
        sb.append(node.right instanceof BinaryExpression ? ")" : "");
        return sb.toString();        
    }
    @Override
    public String visitUnaryExpression(UnaryExpression node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(node.operator);
        sb.append(node.operand instanceof BinaryExpression ? "(" : "");
        sb.append(node.operand.accept(this));
        sb.append(node.operand instanceof BinaryExpression ? ")" : "");
        return sb.toString();        
    }
    @Override
    public String visitCall(Call node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(node.callee.accept(this));
        sb.append("(");
        sb.append(node.arguments.accept(this));
        sb.append(")");
        return sb.toString();
    }
    @Override
    public String visitIdentifier(Identifier node) {
        return node.name;
    }
    @Override
    public String visitParameters(Parameters node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(StreamSupport
                .stream(node.parameters.spliterator(), false)
                .map(identifier -> identifier.accept(this))
                .collect(Collectors.joining(", ")));
        return sb.toString();
    }
    @Override
    public String visitArguments(Arguments node) {
        final StringBuilder sb = new StringBuilder();
        sb.append(StreamSupport
                .stream(node.arguments.spliterator(), false)
                .map(expression -> expression.accept(this))
                .collect(Collectors.joining(", ")));
        return sb.toString();
    }
}
