package sca.ast;

public class Program extends Node {
    public final Iterable<FunctionDeclaration> functions;
    public Program(Iterable<FunctionDeclaration> functions) {
        this.functions = functions;
    }
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitProgram(this);
    }
    @Override
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitProgram(this);
    }
}
