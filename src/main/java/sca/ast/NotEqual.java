package sca.ast;

public class NotEqual extends BinaryExpression {
    public NotEqual(Expression left, Expression right) {
        super(left, right, "!=");
    }
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitNotEqual(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitNotEqual(this);
    }
}
