package sca.ast;

public class WhileStatement extends Statement {
    public final Statement body;
    public final Expression condition;
    public WhileStatement(Statement body, Expression condition) {
        this.body = body;
        this.condition = condition;
    }
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitWhileStatement(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitWhileStatement(this);
    }
}
