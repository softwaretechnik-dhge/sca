package sca.ast;

public class UnaryExpression extends Expression{
    public final Expression operand;
    public final String operator;
    public UnaryExpression(Expression operand, String operator) {
        this.operator = operator;
        this.operand = operand;
    }
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitUnaryExpression(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitUnaryExpression(this);
    }
}
