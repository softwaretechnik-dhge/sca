package sca.ast;

public class Subtract extends BinaryExpression {
    public Subtract(Expression left, Expression right) {
        super(left, right, "-");
    }
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitSubtract(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitSubtract(this);
    }
}
