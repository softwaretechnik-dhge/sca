package sca.ast;

public class Not extends UnaryExpression {
    public Not(Expression operand) {
        super(operand, "!");
    }
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitNot(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitNot(this);
    }
}
