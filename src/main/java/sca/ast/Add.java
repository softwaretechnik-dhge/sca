package sca.ast;

public class Add extends BinaryExpression {
    public Add(Expression left, Expression right) {
        super(left, right, "+");
    }
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitAdd(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitAdd(this);
    }
}
