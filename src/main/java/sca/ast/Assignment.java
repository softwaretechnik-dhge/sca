package sca.ast;

public class Assignment extends Expression {
    public final AssignableExpression target;
    public final Expression source;
    public Assignment(AssignableExpression target, Expression source) {
        this.target = target;
        this.source = source;
    }
    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitAssignment(this);
    }
    public void accept(VisitorWithException visitor) throws Exception {
        visitor.visitAssignment(this);
    }
}
