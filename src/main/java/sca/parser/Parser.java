package sca.parser;

import java.util.LinkedList;

import sca.ast.FunctionDeclaration;
import sca.ast.Statement;
import sca.ast.Identifier;
import sca.ast.Program;
import sca.ast.Block;
import sca.ast.Parameters;
import sca.ast.Expression;
import sca.ast.IfElseStatement;
import sca.ast.WhileStatement;
import sca.ast.Arguments;
import sca.ast.Assignment;
import sca.ast.ReturnStatement;
import sca.ast.Number;
import sca.ast.Minus;
import sca.ast.Not;
import sca.ast.Add;
import sca.ast.Subtract;
import sca.ast.Multiply;
import sca.ast.Divide;
import sca.ast.Less;
import sca.ast.Greater;
import sca.ast.Equal;
import sca.ast.NotEqual;
import sca.ast.Call;

public class Parser {
    private static boolean isExpressionStart(Token token) {
        return token == Token.IDENTIFIER ||
                token == Token.MINUS ||
                token == Token.NOT ||
                token == Token.INTEGER ||
                token == Token.LPAREN;
    }    
    private static boolean isStatementStart(Token token) {
        return token == Token.IF ||
                token == Token.WHILE ||
                token == Token.LBRACE ||
                isExpressionStart(token);
    }

    private Scanner scanner;
    private Token token;

    private void expect(Token token) throws ParserException {
        if (this.token == token) {
            this.token = scanner.nextToken();
        } else {
            throw new ParserException();
        }
    }

    public Program parse(String source) throws ParserException {
        scanner = new Scanner(source);
        token = scanner.nextToken();
        final Program node = program();
        if (token != Token.EOS) {
            throw new ParserException();
        }
        return node;
    }

    // TODO: modify rules to function ::= identifier '(' parameters ')' block
    // TODO: constrain left-hand side in assignment syntax rule explicitly

    // program ::= function { function }
    public Program program() throws ParserException {
        final LinkedList<FunctionDeclaration> functions = new LinkedList<>();
        while (token == Token.IDENTIFIER) {
            functions.add(function());
        }
        return new Program(functions);
    }
    // function ::= identifier '(' parameters ')'
    //              '{' { statement } 'return' expression ';' '}'
    public FunctionDeclaration function() throws ParserException {
        final LinkedList<Statement> statements = new LinkedList<>();
        final Identifier name;
        if (token == Token.IDENTIFIER) {
            name = new Identifier(scanner.getTokenValue());
            token = scanner.nextToken();
        } else {
            throw new ParserException();
        }
        expect(Token.LPAREN);
        final Parameters parameters = parameters();
        expect(Token.RPAREN);
        expect(Token.LBRACE);
        while (isStatementStart(token)) {
            statements.add(statement());
        }
        expect(Token.RETURN);
        statements.add(new ReturnStatement(expression()));
        expect(Token.SEMICOLON);
        expect(Token.RBRACE);
        return new FunctionDeclaration(
                name,
                parameters,
                new Block(statements));
    }
    // parameters ::= [ identifier { ',' identifier } ]
    public Parameters parameters() throws ParserException {
        final LinkedList<Identifier> parameters = new LinkedList<>();
        if (token == Token.IDENTIFIER) {
            parameters.add(new Identifier(scanner.getTokenValue()));
            token = scanner.nextToken();
            while (token == Token.COMMA) {
                token = scanner.nextToken();
                if (token == Token.IDENTIFIER) {
                    parameters.add(new Identifier(scanner.getTokenValue()));
                    token = scanner.nextToken();
                } else {
                    throw new ParserException();
                }
            }
        }
        return new Parameters(parameters);
    }
    // block ::= '{' { statement } '}'
    public Block block() throws ParserException {
        final LinkedList<Statement> statements = new LinkedList<>();
        expect(Token.LBRACE);
        while (isStatementStart(token)) {
            statements.add(statement());
        }
        expect(Token.RBRACE);
        return new Block(statements);
    }
    // statement ::= 'if' '(' expression ')' statement [ 'else' statement ]
    //               | 'while' '(' expression ')' statement
    //               | expression ';'
    //               | block
    public Statement statement() throws ParserException {
        final Statement node;
        if (token == Token.IF) {
            token = scanner.nextToken();
            expect(Token.LPAREN);
            final Expression condition = expression();
            expect(Token.RPAREN);
            final Statement ifBranch = statement();
            Statement elseBranch = null;
            if (token == Token.ELSE) {
                token = scanner.nextToken();
                elseBranch = statement();
            }
            node = new IfElseStatement(ifBranch, elseBranch, condition);
        } else if (token == Token.WHILE) {
            token = scanner.nextToken();
            expect(Token.LPAREN);
            final Expression condition = expression();
            expect(Token.RPAREN);
            node = new WhileStatement(statement(), condition);
        } else if (isExpressionStart(token)) {
            node = expression();
            expect(Token.SEMICOLON);
        } else if (token == Token.LBRACE) {
            node = block();
        } else {
            throw new ParserException();
        }
        return node;
    }
    // expression ::= relation [ '=' expression ]
    public Expression expression() throws ParserException {
        Expression node = relation();
        if (token == Token.ASSIGN) {
            if (node instanceof Identifier) {
                Identifier identifier = (Identifier) node;
                token = scanner.nextToken();
                node = new Assignment(identifier, expression());
            } else {
                throw new ParserException();
            }
        }
        return node;
    }
    // relation ::= operation { ('=='|'!='|'<'|'>') operation }
    public Expression relation() throws ParserException {
        Expression node = operation();
        while (token == Token.EQ ||
                token == Token.NE ||
                token == Token.LT ||
                token == Token.GT) {
            if (token == Token.EQ) {
                token = scanner.nextToken();
                node = new Equal(node, operation());
            } else if (token == Token.NE) {
                token = scanner.nextToken();
                node = new NotEqual(node, operation());
            } else if (token == Token.LT) {
                token = scanner.nextToken();
                node = new Less(node, operation());
            } else if (token == Token.GT) {
                token = scanner.nextToken();
                node = new Greater(node, operation());
            }
        }
        return node;
    }
    // operation ::= term { ('+'|'-') term }
    public Expression operation() throws ParserException {
        Expression node = term();
        while (token == Token.PLUS || token == Token.MINUS) {
            if (token == Token.PLUS) {
                token = scanner.nextToken();
                node = new Add(node, term());
            } else if (token == Token.MINUS) {
                token = scanner.nextToken();
                node = new Subtract(node, term());
            }
        }
        return node;
    }
    // term ::= factor { ('*'|'/') factor }
    public Expression term() throws ParserException {
        Expression node = factor();
        while (token == Token.STAR || token == Token.SLASH) {
            if (token == Token.STAR) {
                token = scanner.nextToken();
                node = new Multiply(node, factor());
            } else if (token == Token.SLASH) {
                token = scanner.nextToken();
                node = new Divide(node, factor());
            }
        }
        return node;
    }
    // factor ::= [ '-' | '!' ] factor | postfix
    public Expression factor() throws ParserException {
        Expression node;
        if (token == Token.MINUS) {
            token = scanner.nextToken();
            node = new Minus(factor());
        } else if (token == Token.NOT) {
            token = scanner.nextToken();
            node = new Not(factor());
        } else {
            node = postfix();
        }
        return node;
    }
    // postfix ::= primary { '(' arguments ')' }
    public Expression postfix() throws ParserException {
        Expression node = primary();
        while (token == Token.LPAREN) {
            token = scanner.nextToken();
            node = new Call(arguments(), node);
            expect(Token.RPAREN);
        }
        return node;
    }
    // primary ::= identifier | number | '(' expression ')'
    public Expression primary() throws ParserException {
        Expression node;
        if (token == Token.IDENTIFIER) {
            node = new Identifier(scanner.getTokenValue());
            token = scanner.nextToken();
        } else if (token == Token.INTEGER) {
            node = new Number(Integer.valueOf(scanner.getTokenValue()));
            token = scanner.nextToken();
        } else if (token == Token.LPAREN) {
            token = scanner.nextToken();
            node = expression();
            expect(Token.RPAREN);
        } else {
            throw new ParserException();
        }
        return node;
    }
    // arguments ::= [ expression { ',' expression } ]
    public Arguments arguments() throws ParserException {
        final LinkedList<Expression> arguments = new LinkedList<>();
        if (isExpressionStart(token)) {
            arguments.add(expression());
            while (token == Token.COMMA) {
                token = scanner.nextToken();
                if (isExpressionStart(token)) {
                    arguments.add(expression());
                } else {
                    throw new ParserException();
                }
            }
        }
        return new Arguments(arguments);
    }
}
