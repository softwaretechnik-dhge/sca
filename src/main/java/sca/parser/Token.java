package sca.parser;

public enum Token {
    LPAREN,
    RPAREN,
    LBRACE,
    RBRACE,
    PLUS,
    MINUS,
    STAR,
    SLASH,
    NOT,
    LT,
    GT,
    EQ,
    NE,
    SEMICOLON,
    COMMA,
    ASSIGN,
    IF,
    ELSE,
    WHILE,
    RETURN,
    IDENTIFIER,
    INTEGER,
    EOS,
    OTHER
}
