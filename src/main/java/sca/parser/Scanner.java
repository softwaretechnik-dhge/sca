package sca.parser;

public class Scanner {
    private static Token lookup(String string) {
        switch (string) {
            case "if": return Token.IF;
            case "else": return Token.ELSE;
            case "while": return Token.WHILE;
            case "return": return Token.RETURN;
            default: return Token.IDENTIFIER;
        }
    }
    private static boolean isDigit(int ch) {
        return ch >= '0' && ch <= '9';
    }
    private static boolean isWhitespace(int ch) {
        return ch == ' ' || ch == '\t' || ch == '\r' || ch == '\n';
    }
    private static boolean isLetter(int ch) {
        return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z');
    }
    private static boolean isIdentifierStart(int ch) {
        return isLetter(ch);
    }
    private static boolean isIdentifierPart(int ch) {
        return isIdentifierStart(ch) || isDigit(ch) || ch == '_';
    }

    private final String source;
    private StringBuilder tokenValue;
    private int cursor;

    public Scanner(String source) {
        this.source = source;
        this.cursor = 0;
    }

    private int next() {
        return cursor < source.length() ? source.charAt(cursor++) : -1;
    }
    private int peek() {
        return cursor < source.length() ? source.charAt(cursor) : -1;
    }

    public String getTokenValue() {
        return tokenValue.toString();
    }
    public Token nextToken() {
        int ch = next();
        while (isWhitespace(ch)) {
            ch = next();
        }
        if (isIdentifierStart(ch)) {
            tokenValue = new StringBuilder();
            tokenValue.append((char) ch);
            while (isIdentifierPart(peek())) {
                tokenValue.append((char) next());
            }
            return lookup(getTokenValue());
        } else if (isDigit(ch)) {
            tokenValue = new StringBuilder();
            tokenValue.append((char) ch);
            while (isDigit(peek())) {
                tokenValue.append((char) next());
            }
            return Token.INTEGER;
        } else if (ch == -1) {
            return Token.EOS;
        } else {
            Token token;
            switch (ch) {
                case '=':
                    if (peek() == '=') {
                        token = Token.EQ;
                        next();
                    } else {
                        token = Token.ASSIGN;
                    }
                    return token;
                case '!':
                    if (peek() == '=') {
                        token = Token.NE;
                        next();
                    } else {
                        token = Token.NOT;
                    }
                    return token;
                case '(':
                    return Token.LPAREN;
                case ')':
                    return Token.RPAREN;
                case '{':
                    return Token.LBRACE;
                case '}':
                    return Token.RBRACE;
                case '+':
                    return Token.PLUS;
                case '-':
                    return Token.MINUS;
                case '*':
                    return Token.STAR;
                case '/':
                    return Token.SLASH;
                case '<':
                    return Token.LT;
                case '>':
                    return Token.GT;
                case ';':
                    return Token.SEMICOLON;
                case ',':
                    return Token.COMMA;
                default:
                    return Token.OTHER;
            }
        }
    }
}
