package sca.dfa;

import sca.cfg.IntraproceduralControlFlowGraph;
import sca.cfg.Node;

public abstract class IntraproceduralForwardAnalysis<T>
        extends IntraproceduralDataFlowAnalysis<T> {
    public IntraproceduralControlFlowGraph cfg;
    public IntraproceduralForwardAnalysis(
            IntraproceduralControlFlowGraph cfg) {
        this.cfg = cfg;
    }
    public Iterable<Node> nodes() {
        return cfg.nodes();
    }
    public Node startNode() {
        return cfg.entryNode();
    }
    public Iterable<Node> outgoing(Node node) {
        return node.successors;
    }
    public Iterable<Node> incoming(Node node) {
        return node.predecessors;
    }
}
