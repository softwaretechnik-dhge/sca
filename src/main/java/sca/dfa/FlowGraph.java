package sca.dfa;

import sca.cfg.Node;

public interface FlowGraph {
    public Iterable<Node> nodes();
    public Node startNode();
    public Iterable<Node> outgoing(Node node);
    public Iterable<Node> incoming(Node node);
} 
