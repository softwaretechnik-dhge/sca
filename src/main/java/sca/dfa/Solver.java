package sca.dfa;

import java.util.Map;

import sca.cfg.Node;

public interface Solver {
    public <T> Map<Node, T> solve(IntraproceduralDataFlowAnalysis<T> analysis);
}
  