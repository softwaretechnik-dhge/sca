package sca.dfa;

import sca.cfg.IntraproceduralControlFlowGraph;
import sca.cfg.Node;

public abstract class IntraproceduralBackwardAnalysis<T>
        extends IntraproceduralDataFlowAnalysis<T> {
    public IntraproceduralControlFlowGraph cfg;
    public IntraproceduralBackwardAnalysis(
            IntraproceduralControlFlowGraph cfg) {
        this.cfg = cfg;
    }
    public Iterable<Node> nodes() {
        return cfg.nodes();
    }
    public Node startNode() {
        return cfg.exitNode();
    }
    public Iterable<Node> outgoing(Node node) {
        return node.predecessors;
    }
    public Iterable<Node> incoming(Node node) {
        return node.successors;
    }
}
