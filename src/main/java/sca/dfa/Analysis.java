package sca.dfa;

import sca.cfg.Node;

public abstract class Analysis<T> {
    public abstract T initElement();
    public abstract T oneElement();
    public abstract T apply(Node node, T incoming);
    public abstract boolean equals(T left, T right);
    public abstract T merge(T left, T right);
}
