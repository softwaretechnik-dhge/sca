package sca.dfa;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import sca.cfg.Node;
import sca.cfg.utilities.LabelPrinter;

public class RoundRobinSolver implements Solver {
    private static final LabelPrinter labelPrinter = new LabelPrinter();
    private static final Logger log =
            LogManager.getLogger(RoundRobinSolver.class);

    public <T> Map<Node, T> solve(
            IntraproceduralDataFlowAnalysis<T> analysis) {
        final Map<Node, T> solution = new HashMap<>();
        log.debug("Data flow analysis: initializing ...");
        for (Node node : analysis.nodes()) {
            if (node != analysis.startNode()) {
                solution.put(node, analysis.oneElement());
            } else {
                solution.put(node, analysis.initElement());
            }
            log.debug("Data flow analysis: derived [["
                    + node.accept(labelPrinter) + "]]"
                    + " = " + solution.get(node));
        }
        boolean stable = true;
        int i = 1;
        do {
            log.debug("Data flow analysis: iteration " + i++);
            stable = true;
            for (Node node : analysis.nodes()) {
                if (node != analysis.startNode()) {
                    T incoming = analysis.oneElement(); 
                    for (Node in : analysis.incoming(node)) {
                        incoming = analysis.merge(incoming, solution.get(in));
                    }
                    final T information = analysis.apply(node, incoming);
                    if (!analysis.equals(solution.get(node), information)) {
                        solution.put(node, information);
                        stable = false;
                    }
                    log.debug("Data flow analysis: derived [["
                            + node.accept(labelPrinter) + "]]"
                            + " =  " + solution.get(node));
                }
            }
        } while (!stable);
        return solution;
    }
}  
