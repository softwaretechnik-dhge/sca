package sca.dfa;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import sca.cfg.Node;
import sca.cfg.utilities.LabelPrinter;

public class WorklistSolver implements Solver {
    private static final LabelPrinter labelPrinter = new LabelPrinter();
    private static final Logger log =
            LogManager.getLogger(RoundRobinSolver.class);

    public <T> Map<Node, T> solve(
            IntraproceduralDataFlowAnalysis<T> analysis) {
        final LinkedList<Node> worklist = new LinkedList<>();
        final Map<Node, T> solution = new HashMap<>();
        log.debug("Data flow analysis: initializing ...");
        for (Node node : analysis.nodes()) {
            if (node != analysis.startNode()) {
                solution.put(node, analysis.oneElement());
                worklist.add(node);
            } else {
                solution.put(node, analysis.initElement());
            }
            log.debug("Data flow analysis: derived [["
                    + node.accept(labelPrinter) + "]]"
                    + " = " + solution.get(node));
        }
        while (!worklist.isEmpty()) {
            Node node = worklist.pop();
            log.debug("Data flow analysis: processing "
                    + node.accept(labelPrinter));
            T incoming = analysis.oneElement(); 
            for (Node in : analysis.incoming(node)) {
                incoming = analysis.merge(incoming, solution.get(in));
            }
            final T information = analysis.apply(node, incoming);
            if (!analysis.equals(solution.get(node), information)) {
                for (Node successor : analysis.outgoing(node)) {
                    worklist.add(successor);
                }
                solution.put(node, information);
            }
            log.debug("Data flow analysis: derived [["
                    + node.accept(labelPrinter) + "]]"
                    + " =  " + solution.get(node));
        }
        return solution;
    }
}  
