package sca.dfa.analyses;

import java.util.Set;
import java.util.HashSet;

import sca.cfg.IntraproceduralControlFlowGraph;
import sca.dfa.IntraproceduralForwardAnalysis;
import sca.cfg.Node;
import sca.cfg.VariableDefinition;

public class ReachingDefinitionAnalysis
        extends IntraproceduralForwardAnalysis<Set<Definition>> {
    public ReachingDefinitionAnalysis(IntraproceduralControlFlowGraph cfg) {
        super(cfg);
    }
    @Override
    public Set<Definition> oneElement() {
        return new HashSet<>();
    }
    @Override
    public Set<Definition> initElement() {
        return new HashSet<>();
    }
    @Override
    public Set<Definition> apply(Node node, Set<Definition> incoming) {
        HashSet<Definition> newSet = new HashSet<>(incoming);
        if (node instanceof VariableDefinition) {
            VariableDefinition current = (VariableDefinition) node;
            newSet.removeIf((other) -> other.variable.equals(current.variable));
            newSet.add(new Definition(current));
        }
        return newSet;
    }
    @Override
    public boolean equals(Set<Definition> left, Set<Definition> right) {
        return (left.size() == right.size() && left.containsAll(right));
    }
    @Override
    public Set<Definition> merge(Set<Definition> left, Set<Definition> right) {
        HashSet<Definition> union = new HashSet<>(left);
        union.addAll(right);
        return union; 
    }
}
