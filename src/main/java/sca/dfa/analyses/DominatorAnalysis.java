package sca.dfa.analyses;

import java.util.Set;
import java.util.HashSet;

import sca.cfg.IntraproceduralControlFlowGraph;
import sca.dfa.IntraproceduralForwardAnalysis;
import sca.cfg.Node;

public class DominatorAnalysis extends IntraproceduralForwardAnalysis<Set<Integer>> {
    public DominatorAnalysis(IntraproceduralControlFlowGraph cfg) {
        super(cfg);
    }
    @Override
    public Set<Integer> initElement() {
        HashSet<Integer> initElement = new HashSet<>();
        initElement.add(cfg.entryNode().id);
        return initElement;
    }
    @Override
    public Set<Integer> oneElement() {
        HashSet<Integer> oneElement = new HashSet<>();
        for (Node node : cfg.nodes()) {
            oneElement.add(node.id);
        }
        return oneElement;
    }
    @Override
    public Set<Integer> apply(Node node, Set<Integer> incoming) {
        HashSet<Integer> newSet = new HashSet<>(incoming);
        newSet.add(node.id);
        return newSet;
    }
    @Override
    public boolean equals(Set<Integer> left, Set<Integer> right) {
        return (left.size() == right.size() && left.containsAll(right));
    }
    @Override
    public Set<Integer> merge(Set<Integer> left, Set<Integer> right) {
        HashSet<Integer> intersection = new HashSet<>(left);
        intersection.retainAll(right);
        return intersection; 
    }
}
