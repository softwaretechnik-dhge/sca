package sca.dfa.analyses.udchains;

import java.util.Map;
import java.util.Set;

import sca.cfg.IntraproceduralControlFlowGraph;
import sca.cfg.Node;
import sca.cfg.utilities.CFGDotPrinter;
import sca.dfa.analyses.Definition;

public class UDChainsDotPrinter extends CFGDotPrinter {
    private static Map<Node, Set<Definition>> udchains;

    public static String print(IntraproceduralControlFlowGraph cfg,
            Map<Node, Set<Definition>> udchains) {
        UDChainsDotPrinter.udchains = udchains;
        final StringBuilder sb = new StringBuilder();
        sb.append("digraph {");
        sb.append(new UDChainsDotPrinter().visitNode(cfg.entryNode()));
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String visitNode(Node node) {
        StringBuilder sb = new StringBuilder();
        sb.append(super.visitNode(node));
        for (Definition definition : udchains.get(node)) {
            sb.append(node.id);
            sb.append("->");
            sb.append(definition.node.id);
            sb.append("[fontcolor=red,color=red,style=dashed,label=\"");
            sb.append(definition.variable);
            sb.append("\"];");
        }
        return sb.toString();
    }
}
