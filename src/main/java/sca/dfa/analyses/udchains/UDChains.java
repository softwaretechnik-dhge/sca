package sca.dfa.analyses.udchains;

import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import sca.cfg.IntraproceduralControlFlowGraph;
import sca.dfa.analyses.Definition;
import sca.dfa.analyses.VariableUseCollector;
import sca.dfa.WorklistSolver;
import sca.dfa.analyses.ReachingDefinitionAnalysis;
import sca.cfg.Node;

public class UDChains {
    private final VariableUseCollector collector = new VariableUseCollector();
    private final Map<Node, Set<Definition>> definitions;

    private Set<Definition> chain(Node node) {
        HashSet<Definition> chain = new HashSet<>();
        for (Node predecessor : node.predecessors) {
            for (Definition definition : definitions.get(predecessor)) {
                if (node.accept(collector).contains(definition.variable)) {
                    chain.add(definition);
                }
            }
        }
        return chain;
    } 

    public UDChains(IntraproceduralControlFlowGraph cfg) {
        definitions = new WorklistSolver().solve(
                new ReachingDefinitionAnalysis(cfg));
    }

    public Map<Node, Set<Definition>> chains() {
        Map<Node, Set<Definition>> chains = new HashMap<>();
        for (Node node : definitions.keySet()) {
            chains.put(node, chain(node));
        }
        return chains;
    }
}
