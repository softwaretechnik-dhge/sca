package sca.dfa.analyses;

import java.util.Map;

import sca.cfg.Add;
import sca.cfg.BinaryOperation;
import sca.cfg.Divide;
import sca.cfg.Equal;
import sca.cfg.Greater;
import sca.cfg.Less;
import sca.cfg.Minus;
import sca.cfg.Multiply;
import sca.cfg.Not;
import sca.cfg.NotEqual;
import sca.cfg.Subtract;

public class Expression {
    private static final Map<Class<?>, String> operations = Map.of(
            Less.class, "<",
            Greater.class, ">",
            Equal.class, "==",
            NotEqual.class, "!=",
            Add.class, "+",
            Subtract.class, "-",
            Multiply.class, "*",
            Divide.class, "/",
            Minus.class, "-",
            Not.class, "!");

    public final BinaryOperation node;
    public Expression(BinaryOperation node) {
        this.node = node;
    }
    @Override
    public boolean equals(Object other) {
        return other.getClass() == Expression.class
                && toString().equals(((Expression) other).toString());
    }
    @Override
    public int hashCode() {
        return toString().hashCode();
    }
    @Override
    public String toString() {
        return node.left
            + operations.get(node.getClass())
            + node.right;
    }
}
