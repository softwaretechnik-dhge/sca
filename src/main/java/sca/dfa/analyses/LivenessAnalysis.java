package sca.dfa.analyses;

import java.util.Set;
import java.util.HashSet;

import sca.cfg.IntraproceduralControlFlowGraph;
import sca.dfa.IntraproceduralBackwardAnalysis;
import sca.cfg.Node;
import sca.cfg.Variable;
import sca.cfg.VariableDefinition;

public class LivenessAnalysis
        extends IntraproceduralBackwardAnalysis<Set<Variable>> {
    public LivenessAnalysis(IntraproceduralControlFlowGraph cfg) {
        super(cfg);
    }
    @Override
    public Set<Variable> initElement() {
        return new HashSet<>();
    }
    @Override
    public Set<Variable> oneElement() {
        return new HashSet<>();
    }
    @Override
    public Set<Variable> apply(Node node, Set<Variable> incoming) {
        HashSet<Variable> newSet = new HashSet<>(incoming);
        if (node instanceof VariableDefinition) {
            newSet.removeIf(
                    (other) -> other.equals(((VariableDefinition) node).variable));
        }
        newSet.addAll(node.accept(new VariableUseCollector()));
        return newSet;
    }
    @Override
    public boolean equals(Set<Variable> left, Set<Variable> right) {
        return (left.size() == right.size() && left.containsAll(right));
    }
    @Override
    public Set<Variable> merge(Set<Variable> left, Set<Variable> right) {
        HashSet<Variable> union = new HashSet<>(left);
        union.addAll(right);
        return union; 
    }
}
