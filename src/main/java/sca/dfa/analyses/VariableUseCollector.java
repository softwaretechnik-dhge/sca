package sca.dfa.analyses;

import java.util.HashSet;
import java.util.Set;

import sca.cfg.BinaryOperation;
import sca.cfg.Branch;
import sca.cfg.Call;
import sca.cfg.Operand;
import sca.cfg.Return;
import sca.cfg.SimpleAssignment;
import sca.cfg.UnaryOperation;
import sca.cfg.Variable;
import sca.cfg.Visitor;
import sca.cfg.Node;

public class VariableUseCollector extends Visitor<Set<Variable>> {
    @Override
    public Set<Variable> visitSimpleAssignment(SimpleAssignment node) {
        return visitOperand(node.value);
    }
    @Override
    public Set<Variable> visitBinaryOperation(BinaryOperation node) {
        HashSet<Variable> newSet = new HashSet<>(visitOperand(node.left));
        newSet.addAll(visitOperand(node.right));
        return newSet;
    }
    @Override
    public Set<Variable> visitUnaryOperation(UnaryOperation node) {
        return visitOperand(node.operand);
    }
    @Override
    public Set<Variable> visitReturn(Return node) {
        return visitOperand(node.value);
    }
    @Override
    public Set<Variable> visitBranch(Branch node) {
        return visitOperand(node.value);
    }
    @Override
    public Set<Variable> visitCall(Call node) {
        HashSet<Variable> newSet = new HashSet<>(visitOperand(node.target));
        for (Operand argument : node.arguments) {
            newSet.addAll(visitOperand(argument));
        }
        return newSet;
    }
    @Override
    public Set<Variable> visitNode(Node node) { return new HashSet<>(); }
    public Set<Variable> visitOperand(Operand operand) {
        return operand instanceof Variable
                ? new HashSet<>() {{ add((Variable) operand); }}
                : new HashSet<Variable>();
    }
}
