package sca.dfa.analyses;

import java.util.Set;
import java.util.HashSet;

import sca.cfg.IntraproceduralControlFlowGraph;
import sca.dfa.IntraproceduralForwardAnalysis;
import sca.cfg.Node;
import sca.cfg.BinaryOperation;
import sca.cfg.VariableDefinition;

public class AvailableExpressionAnalysis
        extends IntraproceduralForwardAnalysis<Set<Expression>> {
    public AvailableExpressionAnalysis(IntraproceduralControlFlowGraph cfg) {
        super(cfg);
    }
    @Override
    public Set<Expression> initElement() {
        return new HashSet<>();
    }
    @Override
    public Set<Expression> oneElement() {
        HashSet<Expression> oneElement = new HashSet<>();
        for (Node node : cfg.nodes()) {
            if (node instanceof BinaryOperation) {
                oneElement.add(new Expression((BinaryOperation) node));
            }
        }
        return oneElement;
    }
    @Override
    public Set<Expression> apply(Node node, Set<Expression> incoming) {
        HashSet<Expression> newSet = new HashSet<>(incoming);
        if (node instanceof VariableDefinition) {
            VariableDefinition current = (VariableDefinition) node;
            newSet.removeIf(
                    (other) -> other.node.left.equals(current.variable)
                            || other.node.right.equals(current.variable));
        }
        if (node instanceof BinaryOperation) {
            BinaryOperation current = (BinaryOperation) node;
            if (!current.left.equals(current.variable)
                    && !current.right.equals(current.variable)) {
                newSet.add(new Expression(current));
            }
        }
        return newSet;
    }
    @Override
    public boolean equals(Set<Expression> left, Set<Expression> right) {
        return (left.size() == right.size() && left.containsAll(right));
    }
    @Override
    public Set<Expression> merge(Set<Expression> left, Set<Expression> right) {
        HashSet<Expression> intersection = new HashSet<>(left);
        intersection.retainAll(right);
        return intersection; 
    }
}
