package sca.dfa.analyses;

import sca.cfg.Variable;
import sca.cfg.VariableDefinition;

public class Definition {
    public final VariableDefinition node;
    public final Variable variable;
    public Definition(VariableDefinition node) {
        this.node = node;
        variable = node.variable;
    }
    @Override
    public boolean equals(Object other) {
        return other.getClass() == Definition.class
                && node.id == ((Definition) other).node.id;
    }
    @Override
    public int hashCode() {
        return node.id;
    }
    @Override
    public String toString() {
        return "(" + variable + "," + node.id + ")";
    }
}
