# 1.0.0

- initial version

# 1.0.1

- added typechecking

# 1.0.2

- added intraprocedural control flow graph

# 1.0.3

- added data flow analysis
